class Group < ActiveRecord::Base
  has_many :authorizes
  has_many :users, :through => :authorizes
  has_many :visibilities
  has_many :categories, :through => :visibilities
end
