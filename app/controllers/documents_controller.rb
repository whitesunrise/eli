class DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
 # before_action :authenticate_user!, :except => [:index]
  before_action :authenticate_user!
  # GET /documents
  # GET /documents.json
  def index
  #  if current_user.id == 6
  #    base_query = "SELECT public.documents.* from  public.documents" 
  #  else
      base_query = "SELECT distinct public.documents.*
        FROM public.documents INNER JOIN public.categorizations ON public.documents.id = public.categorizations.document_id
          INNER JOIN public.categories ON public.categories.id = public.categorizations.category_id
          INNER JOIN public.visibilities ON public.categories.id = public.visibilities.category_id
          INNER JOIN public.groups ON public.groups.id = public.visibilities.group_id 
          where
          public.groups.id in (select distinct public.authorizes.group_id from public.authorizes where authorizes.user_id = #{current_user.id})"
   # end

    restrictors =[]
    if params['search_term']
      terms = params['search_term'].downcase.split
      terms.each{ |term| restrictors << "LOWER(title) LIKE '%#{term}%' OR LOWER(content) LIKE '%#{term}%' OR LOWER(tags) LIKE '%#{term}%'" }
    end
    
    restrictions = ''
    restrictors.each_with_index{ |restriction, i| 
     if i == 0 
       restrictions = " and #{restriction}"
     else 
        restrictions = " #{restrictions} or #{restriction}"
      end   
    }
Rails.logger.debug "\n\n\n**** base query: #{base_query} ****\n\n\n"
Rails.logger.debug "\n\n\n**** restrictions: #{restrictions} ****\n\n\n"
    @documents = Document.all.find_by_sql("#{base_query}#{restrictions}") 

### categories

   

  end

  # GET /documents/cat/1
  def by_category
    if params['id']
      @documents  = Document.joins(:categories).select("documents.*,categories.name").where("categories.id" => params['id'])   
    end
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
  end

  # GET /documents/new
  def new
    @document = Document.new
  end

  # GET /documents/1/edit
  def edit
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(document_params)

    respond_to do |format|
      if @document.save
        format.html { redirect_to documents_url, notice: 'Document was successfully created.' }
        format.json { render action: 'show', status: :created, location: @document }
      else
        format.html { render action: 'new' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to documents_url, notice: 'Document was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:title, :vlink, :content, :tags, :status, :file_name, :category_ids => [])
    end
end
