json.array!(@documents) do |document|
  json.extract! document, :id, :title, :content, :status
  json.url document_url(document, format: :json)
end
