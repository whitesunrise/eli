class RemoveFilenameFromDocuments < ActiveRecord::Migration
  def change
    remove_column :documents, :file_name
  end
end
