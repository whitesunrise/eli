class ChangeCatidToInteger < ActiveRecord::Migration
  def change
    change_column :sub_categories, :category_id, 'integer USING CAST(category_id AS integer)'
  end
end
