class CreateVisibilities < ActiveRecord::Migration
  def change
    create_table :visibilities do |t|
      t.integer :group_id
      t.integer :category_id

      t.timestamps
    end
  end
end
