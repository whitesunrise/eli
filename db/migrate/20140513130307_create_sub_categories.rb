class CreateSubCategories < ActiveRecord::Migration
  def change
    create_table :sub_categories do |t|
      t.string :category_id
      t.string :integer
      t.string :name

      t.timestamps
    end
  end
end
